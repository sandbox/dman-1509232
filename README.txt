Pathologic Repair
----------

By Dan Morrison
http://coders.co.nz/

----------
This module is an extension moving forward from 'Pathologic'
http://drupal.org/project/pathologic
By Garrett Albright

Pathologic assists in keeping site links together when moving or migrating them, 
but doesn't actually fix the root of the problem - invalid links.
Pathologic repairs links by way of an output filter, repairing a site in a
just-in-time manner. These results are (usually) cached, so the end result is
good enough BUT it would be better to eliminate the errors once-and-for-all 
in the first place.
This is what pathologic_repair attempts to do.

It uses the pathologic link-rewriting features to find problems, but once found
it will wrote back the fixed version into the database, so errors never need be 
repaired again.

It also includes a few extra utilities that will globally repair changes to the 
files database, and embedded links to those files.

----------
Use Case
----------
You have developed a working site on a local machine http://localhost/drupal/
but because you kept a few of the default settings, all your images are being
stored and served out of the path:
http://localhost/drupal/sites/default/files/images/img.gif
When creating the site, (maybe using a WYSIWYG editor) links to those images
are now coded into your content as either the 'fully justified' URL above,
or possibly as just /drupal/sites/default/files/images/img.gif

Now you want to move your site to its real home http://example.com/
and have figured out that it would be best to:
- have drupal served from the root
- set up for multisite, and stop using 'default' and name your Drupal instance
'example.com' (Files should live in /sites/example.com/files)

This is a problem. 
Even image.module and upload.module have encoded the wrong path into the database.

THIS MODULE will help repair all those errors upon migration.

----------
Usage
----------
The simplest fix is to start with some database string replacements. 
This utility is provided through the pathologic settings form.
Enter the old and new paths and the Drupal database 'files' table will be updated.

=======
For full installation and configuration instructions, please see this page in the Drupal manual:
http://drupal.org/node/257026
